//////////////////////////////////////////////////////////////////////////////
// FILE   : MultNormal.Trunc.tol
// PURPOSE: Class @MultNormal.Trunc
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Matrix SamplePrior.BivTNrm(Set priorXY)
//////////////////////////////////////////////////////////////////////////////
{
  Group`("ConcatRows",EvalSet(priorXY,Matrix(@BivTNrm pr)
  {
    Matrix mu  = Col(pr->X.mean, pr->Y.mean);
    Real XYcov = pr->X.sd*pr->Y.sd*pr->XY.cor;
    Matrix Cov = ((pr->X.sd^2, XYcov     ),
                  (XYcov,      pr->Y.sd^2));
    Matrix L   = Choleski(Cov);
    Matrix c1  = Col(pr->X.lower, pr->Y.lower);
    Matrix c2  = Col(pr->X.upper, pr->Y.upper);
    Tra(RandTruncatedMultNormal(mu, L, c1, c2, 1, pr->sampleLength))
  }))
};

//////////////////////////////////////////////////////////////////////////////
Class @MultNormal.Trunc
//////////////////////////////////////////////////////////////////////////////
{
  //SetOfSet of BysPrior::@PsbTrnNrmUnfSclDst (can use DefVar)
  Set varInfo = Copy(Empty);
  
  ////////////////////////////////////////////////////////////////////////////
  //Text with a constraint by line with explicit linear inequations in a 
  //sparse way, i.e., zeroes are not needed
  //  LowerBound <= c[1]*var[1]+...+c[n]*var[n] <= upperBound;
  ////////////////////////////////////////////////////////////////////////////
  Text vectorialInequations = "";
  
  Set _.paramNames = Copy(Empty);
  Real _.n = ?;
  VMatrix _.b.lower = Constant(0,0,?);
  VMatrix _.b.upper = Constant(0,0,?);
  VMatrix _.A = Constant(0,0,?);
  VMatrix _.Ab.lower = Constant(0,0,?);
  VMatrix _.Ab.upper = Constant(0,0,?);
  VMatrix _.b.prior.y = Constant(0,0,?);
  VMatrix _.b.prior.x = Constant(0,0,?);
  VMatrix _.b.prior.nu = Constant(0,0,?);
  VMatrix _.b.prior.sigma = Constant(0,0,?);
  VMatrix _.L = Constant(0,0,?);
  VMatrix _.Li = Constant(0,0,?);
  VMatrix _.C = Constant(0,0,?);
  VMatrix _.c = Constant(0,0,?);
  VMatrix _.D = Constant(0,0,?);
  VMatrix _.d = Constant(0,0,?);
  VMatrix _.b0 = Constant(0,0,?);
  VMatrix _.z0 = Constant(0,0,?);
  Real _.b0.matches = ?;
  Static Set ipoptions = 
  [[
    TolIpopt::@Option("Integer", "print_level", 0)
  ]];
  
  Static Real bigValue = 1E6;
  

  ////////////////////////////////////////////////////////////////////////////
  Real _setup.index(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real _.n := Card(varInfo);
    Set EvalSet(varInfo,Real(BysPrior::@PsbTrnNrmUnfSclDst pr)
    {
      Text name = pr->Name;
      Set PutName(name,pr);  
      1
    });
    Set _.paramNames := EvalSet(varInfo,Text(BysPrior::@PsbTrnNrmUnfSclDst pr)
    {
      Text name = pr->Name;
      Eval(name+"=name")       
    });
    Real SetIndexByName(varInfo); 
    Real SetIndexByName(_.paramNames); 
    True
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Real _setup.bounds(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatrix _.b.lower := Mat2VMat(SetCol(
    EvalSet(varInfo,Real(BysPrior::@PsbTrnNrmUnfSclDst pr)
    {
      If(IsUnknown(pr->LowerBound),-BysPrior::@MultNormal.Trunc::bigValue,
        pr->LowerBound)
    })));
    VMatrix _.b.upper := Mat2VMat(SetCol(
    EvalSet(varInfo,Real(BysPrior::@PsbTrnNrmUnfSclDst pr)
    {
      If(IsUnknown(pr->UpperBound),+BysPrior::@MultNormal.Trunc::bigValue,
        pr->UpperBound)
    })));
    True
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Real _setup.prior(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatrix _.b.prior.nu := Mat2VMat(SetCol(
    EvalSet(varInfo,Real(BysPrior::@PsbTrnNrmUnfSclDst pr)
    {
      If(IsUnknown(pr->Nu),0,pr->Nu)
    })));
    VMatrix _.b.prior.sigma := Mat2VMat(SetCol(
    EvalSet(varInfo,Real(BysPrior::@PsbTrnNrmUnfSclDst pr)
    {
      If(IsUnknown(pr->Sigma),BysPrior::@MultNormal.Trunc::bigValue,
        pr->Sigma)
    })));
    Set hasPriorSigma = Select(Range(1,_.n,1),Real(Real k)
    {
      !IsUnknown(varInfo[k]->Sigma)
    });
    VMatrix _.b.prior.y := SubRow(Mat2VMat(SetCol(
    EvalSet(varInfo,Real(BysPrior::@PsbTrnNrmUnfSclDst pr)
    {
      pr->Nu/pr->Sigma
    }))),hasPriorSigma);
    
    Matrix b.prior.x.tr = If(!_.n,Constant(0,3,?), SetMat(SetConcat(
    For(1,_.n,Set(Real k)
    {
      Real s = varInfo[k]->Sigma; 
      If(IsUnknown(s),Empty,[[ [[k,k,Real 1/s]] ]])
    }))));
    VMatrix _.b.prior.x := SubRow(Convert(
      Triplet(b.prior.x.tr,_.n,_.n),"Cholmod.R.Sparse"),hasPriorSigma);
    True
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Real _setup.polytope(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    NameBlock in=BysPrior::@MultNormal.Trunc::ParseInequalities(
      _.paramNames, vectorialInequations);
    VMatrix _.Ab.lower :=in::Ab.lower;
    VMatrix _.A :=in::A;
    VMatrix _.Ab.upper :=in::Ab.upper;
    NameBlock plt = BysPrior::@MultNormal.Trunc::GetPolytope(
     _.b.lower, _.b.upper, _.Ab.lower, _.A, _.Ab.upper);
    VMatrix _.C := plt::C;
    VMatrix _.c := plt::c;
    VMatrix _.L := Eye(_.n,_.n,0,Tra(_.b.prior.sigma));
    VMatrix _.Li := Eye(_.n,_.n,0,Tra(_.b.prior.sigma^-1));
    VMatrix _.D := _.C * _.L;
    VMatrix _.d := _.c - _.C*_.b.prior.nu;
    True
  };    

  ////////////////////////////////////////////////////////////////////////////
  Real _setup.feasiblePoint(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(!_.n,
    {
      VMatrix _.z0 := Constant(_.n,1,0);
      True
    }, 
    {
      VMatrix g0 = _.D*Constant(_.n,1,0)-_.d;
      Real ok0 = VMatMax(g0)<=0;
     
      VMatrix _.z0 := If(ok0, Constant(_.n,1,0),
      {
        WriteLn(
        "[BysPrior::@MultNormal.Trunc] "
        "Mean of prior doesn�t match inequalities!","W");
        BysPrior::@MultNormal.Trunc::FindPointInPolytope(_.D,_.d,1.E-5)
      });
      VMatrix _.b0 := _.L*_.z0+_.b.prior.nu;
      Real _.b0.matches := MatchInequalities(_.b0);
      True 
    })
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Real Setup(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real _setup.index(?);
    Real _setup.bounds(?);
    Real _setup.prior(?);
    Real _setup.polytope(?);
    Real _setup.feasiblePoint(?);
    True
  };
  
  ////////////////////////////////////////////////////////////////////////////
  VMatrix DrawPrior(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(!_.n, Constant(_.n,1,0),
    {
      VMatrix z = TruncStdGaussian(_.D, _.d, _.z0, 1, 1E-5);
      VMatrix _.z0 := z;
      _.L*z+_.b.prior.nu
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix EvalInequalities(VMatrix b)
  ////////////////////////////////////////////////////////////////////////////
  {
    _.C*b-_.c
  };

  ////////////////////////////////////////////////////////////////////////////
  Real MatchInequalities(VMatrix b)
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatMax(EvalInequalities(b))<=0
  };

  ////////////////////////////////////////////////////////////////////////////
  Set DefVar(
    Text Name,       // Unique identidier of the parameter
    Real Nu,         // Average for normal case
    Real Sigma,      // Sigma for normal case and ? or 1/0 for uniform
    Real LowerBound, // -1/0 or ? if there is no lower bound
    Real UpperBound) // +1/0 or ? if there is no upper bound
  ////////////////////////////////////////////////////////////////////////////
  {
    BysPrior::@PsbTrnNrmUnfSclDst(Name,Nu,Sigma,LowerBound,UpperBound)
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Static NameBlock ParseInequalities(
     Set paramNames, 
     Text vectorialInequations)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real n = Card(paramNames);
    If(!TextLength(Compact(vectorialInequations)),
    {[[
      VMatrix Ab.lower = Constant(0,1,?);
      VMatrix A = Constant(0,n,?);
      VMatrix Ab.upper = Constant(0,1,?)
    ]]},
    {
      Set IncludeText(SetSum(For(1,n,Text(Real k)
      {
        "VMatrix "+paramNames[k]+"=SubCol(Eye(n),[["<<k+"]]);\n"
      })));
      Set eqLines = {
        Set tok = Tokenizer(vectorialInequations,";");
        Set aux1 = Select(EvalSet(tok,Compact),TextLength);
        Set aux2 = EvalSet(aux1,Set(Text line) 
        { 
          Tokenizer(Replace(line,"<=","@"),"@") 
        });
        EvalSet(aux2,Set(Set line)
        {[[
          Real    L = Eval(line[1]);
          VMatrix A = Tra(Eval("VMatrix ("<<line[2]+")"));
          Real    U = Eval(line[3])
        ]]})
      };
      [[
        VMatrix Ab.lower = Max(Mat2VMat(SetCol(Traspose(eqLines)[1])),
         -BysPrior::@MultNormal.Trunc::bigValue);
        VMatrix A = Group("ConcatRows",Traspose(eqLines)[2]);
        VMatrix Ab.upper = Min(Mat2VMat(SetCol(Traspose(eqLines)[3])),
          BysPrior::@MultNormal.Trunc::bigValue)
      ]]
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Static NameBlock GetPolytope(
     VMatrix lower, 
     VMatrix upper, 
     VMatrix Lower, 
     VMatrix A, 
     VMatrix Upper)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real n = VRows(lower);
    VMatrix C_ = Eye(n) << -Eye(n) << A << -A;
    VMatrix c_ = upper << -lower << Upper << -Lower;
    Set c.isFinite = MatQuery::SelectRowsWithValue(IsFinite(c_),1);
    [[
      VMatrix C = SubRow(C_,c.isFinite);
      VMatrix c = SubRow(c_,c.isFinite)
    ]]
  };
        
  ////////////////////////////////////////////////////////////////////////////
  Static VMatrix FindPointInPolytope(VMatrix D, VMatrix d_, Real inDst)
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatrix d = d_-Abs(d_)*inDst-Not(d_)*inDst;
    Real n = VColumns(D);
    VMatrix x = Constant(n,1,0);
    Real g = VMatMax(D*x-d_);
    Real nu_try = 0;
  //WriteLn("[FindPointInPolytope] Trivial zero solution try g:"<<g);
    While(And(g>0,nu_try<=n),
    {
      TolIpopt::@SQP opt = { TolIpopt::@SQP::Optimize.Constrained(
        @MultNormal.Trunc::ipoptions,
        VMatrix C_ = Eye(n),
        VMatrix c_ = Rand(n,1,-1,1),
        Real c0_ = Rand(0,0), 
        VMatrix Constant(0,0,?),
        VMatrix Constant(0,0,?),
        VMatrix Constant(0,0,?),
        VMatrix D, 
        VMatrix d) };
      VMatrix x:=opt::_.x;  
      Real g := VMatMax(D*x-d_);
    //WriteLn("[FindPointInPolytope] Random try "<<nu_try+" g:"<<g);
      Real nu_try := nu_try+1
    });
    x
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Static VMatrix DrawInPolytope(VMatrix C, VMatrix c, 
    Real sampleSize, Real inDst)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real n = VColumns(C);
    Real s = Max(VMatMax(Abs(C)),VMatMax(Abs(c)));
    VMatrix L = Eye(n,n,0,s);
    VMatrix D = C * L;
    VMatrix d = c;
    VMatrix z0 = BysPrior::@MultNormal.Trunc::FindPointInPolytope(D,d,inDst);
    VMatrix z = TruncStdGaussian(D, d, z0, sampleSize, inDst);
    L*z
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Static VMatrix TryOrFindPointInPolytope(
     VMatrix D, VMatrix d_, VMatrix x0, Real inDst)
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatrix d = d_-Abs(d_)*inDst-Not(d_)*inDst;
    Real n = VColumns(D);
    VMatrix x_ = x0;
    Real g_ = 1;
    Real If(And(VRows(x_)==VColumns(D),VColumns(x_)==1),
    {
      Real g_ := VMatMax(D*x_-d)
    //WriteLn("[FindPointInPolytope] Propposed solution try g:"<<g)
    });
    Real If(g_>0,
    {
      VMatrix x_ := Constant(n,1,0);
      Real g_ := VMatMax(D*x_-d)
    //WriteLn("[FindPointInPolytope] Trivial zero solution try g:"<<g)
    });
    Real nu_try = 0;
    While(And(g_>0,nu_try<=n),
    {
      TolIpopt::@SQP opt = { TolIpopt::@SQP::Optimize.Constrained(
        @MultNormal.Trunc::ipoptions,
        VMatrix C_ = Eye(n),
        VMatrix c_ = Rand(n,1,-1,1),
        Real c0_ = Rand(0,0), 
        VMatrix Constant(0,0,?),
        VMatrix Constant(0,0,?),
        VMatrix Constant(0,0,?),
        VMatrix D, 
        VMatrix d) };
      VMatrix x_:=opt::_.x;  
      Real g_ := VMatMax(D*x_-d);
    //WriteLn("[FindPointInPolytope] Random try "<<nu_try+" g:"<<g);
      Real nu_try := nu_try+1
    });
    x_
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Static VMatrix DrawInPolytopeFromPoint(VMatrix C, VMatrix c, VMatrix x0, 
    Real sampleSize, Real inDst)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real n = VColumns(C);
    Real s = Max(VMatMax(Abs(C)),VMatMax(Abs(c)))*1E2;
    VMatrix D = C * s;
    VMatrix z0 = BysPrior::@MultNormal.Trunc::
      TryOrFindPointInPolytope(D,c,x0/s,inDst);
    VMatrix z = TruncStdGaussian(D,c,z0,sampleSize,inDst);
    z*s
  };
  
  
  ////////////////////////////////////////////////////////////////////////////
  Static @MultNormal.Trunc Default(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    BysPrior::@MultNormal.Trunc prior = [[
      Set varInfo = Copy(Empty)
    ]];
    Real prior::Setup(?);
    prior
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Static @MultNormal.Trunc Build(
    Set varInfo_,
    Text vectorialInequations_)
  ////////////////////////////////////////////////////////////////////////////
  {
    BysPrior::@MultNormal.Trunc prior = [[
      Set varInfo = varInfo_;
      Text vectorialInequations = vectorialInequations_
    ]];
    Real prior::Setup(?);
    prior
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @MultNormal.Trunc BuildRand.Try(
    Set paramNames, 
    Real signConstraint.ratio,
    Real orderConstraint.number,
    Real arbitraryConstraint.number,
    Real expansionCoefficient)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [@MultNormal.Trunc::BuildRand] 1");
    Real n = Card(paramNames);
    Real ratio = signConstraint.ratio;
    Real mav = BysPrior::@MultNormal.Trunc::bigValue;
    VMatrix sign = Case(
    ratio<=0,LT(Rand(n,1,-1,1),0),
    1==1, 
    {
      Real lambda = 1/((1-ratio)*2);
      VMatrix v = Round(Rand(n,1,-lambda,lambda));
      IfVMat(LT(v,0),-1,IfVMat(GT(v,0),1,0))
    });
    VMatrix lower = IfVMat(GT(sign,0), Constant(n,1,0),Constant(n,1,-mav));
    VMatrix upper = IfVMat(LT(sign,0), Constant(n,1,0),Constant(n,1,+mav));
  //WriteLn("TRACE [@MultNormal.Trunc::BuildRand] 2");
    Set nonNeg = MatQuery::SelectRowsWithValue(GE(sign,0),1);
    Set nonPos = MatQuery::SelectRowsWithValue(LE(sign,0),1);
    Set nonNeg.perm = If(!Card(nonNeg),Empty,
      MatSet(RandPermutation(1, Card(nonNeg)))[1]);
    Set nonPos.perm = If(!Card(nonPos),Empty,
      MatSet(RandPermutation(1, Card(nonPos)))[1]);
    Set all.pairs = For(1,Card(nonNeg.perm)-1, Set(Real k)
    {
      [[ nonNeg[ nonNeg.perm[k] ], nonNeg[ nonNeg.perm[k+1] ] ]]
    }) <<
    For(1,Card(nonPos.perm)-1, Set(Real k)
    {
      [[ nonPos[ nonPos.perm[k] ], nonPos[ nonPos.perm[k+1] ] ]]
    });
    Set all.pairs.perm = If(!Card(all.pairs),Empty,
      MatSet(RandPermutation(1, Card(all.pairs)))[1]);
    Text ineq = SetSum(For(1, orderConstraint.number, Text(Real k)
    {
      Real pair.idx = all.pairs.perm[k];
      Real k1 = all.pairs[pair.idx][1];
      Real k2 = all.pairs[pair.idx][2];
      "-"<<mav<<" <= "<<paramNames[k1]<<"-"<<paramNames[k2]<<" <= 0;\n"
    })) << SetSum(For(1, arbitraryConstraint.number, Text(Real k)
    {
      Real bnd = IntRand(-1.5,1.5);
      Real low = If(bnd<=0,Rand(-mav/4,mav/8),-mav);
      Real upp = Case(
        bnd<0,mav,
        bnd>0,Rand(-mav/8,mav/4),
        bnd==0, Rand(low*1.1,mav/4));
      Real coef1 = Rand(-mav/3,mav/3);
      Real coef2 = Rand(-mav/3,mav/3);
      Text c1 = FormatReal(coef1,"%+.15lg");
      Text c2 = FormatReal(coef2,"%+.15lg");
      ""<<low+" <= "<<paramNames[1]<<"*("<<c1<<") "<<
                "+ "<<paramNames[2]<<"*("<<c2<<") <= "<<upp<<";\n"
    }));
  //WriteLn("TRACE [@MultNormal.Trunc::BuildRand] 3");
    NameBlock in=BysPrior::@MultNormal.Trunc::ParseInequalities(
      paramNames, ineq);
  //WriteLn("TRACE [@MultNormal.Trunc::BuildRand] 4");
    NameBlock plt = BysPrior::@MultNormal.Trunc::GetPolytope(
      lower, upper, in::Ab.lower, in::A, in::Ab.upper);
  //WriteLn("TRACE [@MultNormal.Trunc::BuildRand] 5");
    Real S = 1000;
    VMatrix X = Tra(BysPrior::@MultNormal.Trunc::DrawInPolytope(
     plt::C, plt::c, S, 1E-5));
  //WriteLn("TRACE [@MultNormal.Trunc::BuildRand] 6");
    VMatrix X.nu = Constant(1,S,1/S)*X; 
  //WriteLn("TRACE [@MultNormal.Trunc::BuildRand] 7");
    VMatrix X.NU = Group("ConcatRows",NCopy(S,X.nu));
  //WriteLn("TRACE [@MultNormal.Trunc::BuildRand] 8");
    VMatrix X.var = Constant(1,S,1/S)*(X-X.NU)^2; 
  //WriteLn("TRACE [@MultNormal.Trunc::BuildRand] 9");
    VMatrix X.std = X.var^0.5;
  //WriteLn("TRACE [@MultNormal.Trunc::BuildRand] 10");
    VMatrix prior.sigma = Tra(X.std)$*
      (Rand(n,1,0.05,1.50)^expansionCoefficient);
  //WriteLn("TRACE [@MultNormal.Trunc::BuildRand] 11");
    VMatrix prior.nu = 
    {
      Real r = Rand(0,.50);
      Real k = IntRand(1,S);
      Tra(X.nu*(1-r)+SubRow(X,[[k]])*r)
    };
  //WriteLn("TRACE [@MultNormal.Trunc::BuildRand] 12");
    Set varInfo_ = For(1,n,Set(Real k)
    {
      BysPrior::@PsbTrnNrmUnfSclDst(
        paramNames[k],
        VMatDat(prior.nu,k,1),
        VMatDat(prior.sigma,k,1),
        VMatDat(lower,k,1),
        VMatDat(upper,k,1)
      )
    });
    BysPrior::@MultNormal.Trunc prior = [[
      Set varInfo = varInfo_;
      Text vectorialInequations = ineq
    ]];
    Real prior::Setup(?);
    prior
  };
    
  ////////////////////////////////////////////////////////////////////////////
  Static @MultNormal.Trunc BuildRand(
    Set paramNames, 
    Real signConstraint.ratio,
    Real orderConstraint.number,
    Real arbitraryConstraint.number,
    Real expansionCoefficient)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(!Card(paramNames), BysPrior::@MultNormal.Trunc::Default(?),
    {
      BysPrior::@MultNormal.Trunc try = 
      BysPrior::@MultNormal.Trunc::BuildRand.Try(
        paramNames, 
        signConstraint.ratio,
        orderConstraint.number,
        arbitraryConstraint.number,
        expansionCoefficient);
      Real iter=1;
      While(Not(try::_.b0.matches),
      {
        BysPrior::@MultNormal.Trunc try = 
        BysPrior::@MultNormal.Trunc::BuildRand.Try(
          paramNames, 
          signConstraint.ratio,
          orderConstraint.number,
          arbitraryConstraint.number,
          expansionCoefficient);
        Real iter := iter+1
      });
      try
    })
  }    

};
